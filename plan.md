# Plan

## API (Flask)

### api/add [POST] **Removed**
* Functionality to add n amount of new diapers to total per size


### api/remove [POST] **Removed**
* Functionality to remove n amount of diapers from total per size


### api/diapers [GET] *DONE*
* Get all diapers from all babies associated with user *DONE*
### api/diapers/*babyName* [GET, POST] *DONE*
* *GET* Get all diapers for specific baby *DONE*
* *POST* Create diapers for specific baby (Used by Baby create page) *DONE*
* *POST* Delete all diapers for specific baby
### api/diapers/*babyName*/*size* [GET, POST] *DONE*
* *GET* Get diapers of a certain size *DONE*
* *POST* Create diapers for a certain size *DONE*
* *POST* Update diapers for a certain size *DONE*
* *POST* Delete diapers for a certain size *DONE*

* Example
```json
{
	"status": "success",
	"babyName": 
	{
		"diapers":
		[
			{
				"size": "0",
				"quantity": "93",
				"modified": "09/25/2021"
			},
			
			{
				"size": "1",
				"quantity": "201",
				"modified": "09/25/2021"
			},
			
			{
				"size": "2",
				"quantity": "277",
				"modified": "09/25/2021"
			},

			{
				"size": "3",
				"quantity": "277",
				"modified": "09/25/2021"
			},

			{
				"size": "4",
				"quantity": "277",
				"modified": "09/25/2021"
			}
		]
	}
}
```

### api/auth/login [POST] *DONE*
* Similar to OrderAPI login


### api/auth/register [POST] *DONE*
* Similar to OrderAPI register


### api/addbaby [POST] *DONE*
* Create new baby and associate with user


### api/*username* [GET, POST] *DONE*
* Get and set user info


### api/*username*/*baby* [GET, POST] *DONE*
* Get and set info about user-specific baby


### *Future*
* Images for babies and users
* Social aspect?
	* Friends



## Front-End (React w/ router, SCSS)

### /
* If logged in
	* Info about how many diapers are left for each size
		* Maybe represented as individual cards with 
			* size as the title
			* quantity and last added as data

	* "+" and "-" buttons as well as a field to edit the amount of diapers
* If not logged in
	* Landing page
		* Description
		* Sign up and login buttons


### /size (*Must be logged in*)
* "Use Diaper" button (undo popup for 5 seconds after)
* Indication of what size you're currently on at the top
* Diapers remaining counter that can be edited in a popup modal (click, hold, or separate button)


### /register
* email field
* username field
* password field
* submit button
* Google integration?


### /login
* email field
* password field
* submit button
* Google integration?


### /*username* (*Must be logged in*)
* Personal info displayed in nicely formatted form format
* Edit button that turns some of the fields to active inputs
* List of links to babies associated with the user
* Add baby with modal to add baby
	* Form with baby modal fields

### /*username*/*baby* (*Must be logged in*)
* Baby info displayed in nicely formatted text
* Edit button that turns some of the fields to active inputs


### *Future*
* Add the ability to become friends
* Add ability for other people to view your profile
