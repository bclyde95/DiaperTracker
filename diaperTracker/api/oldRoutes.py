""" Endpoint to add n amount of diapers to Diaper in Inventory """
@api.route('/add', methods=['POST'])
@jwt_required(refresh=False)
def add():
    """ Takes in POST request with babyId, num, and size of diapers, adds them to the current amount and updates the db """

    data = request.get_json()

    # get user by username from jwt token
    user = User.getByUsername(get_jwt_identity())

    # Get baby object from user.babies. If no match, return error message
    for b in user.babies:
        if b.id == data["babyId"]:
            baby = b

    # Get diaper object from user.babies.inventory.
    try:
        for d in baby.inventory.diapers.getInventory():
            if d.size == data["size"]:
                diaper = d
    except KeyError:
        return jsonError('inventory does not exist, add inventory to baby', 404)
        
    # convert data["toAdd"] to int, if ValueError, return error response
    try:
        toAdd = int(data["toAdd"])
    except ValueError:
        return jsonError('toAdd must be an integer', 422)

    # check if user is Null, return error response if so
    if user:
        # check if baby is Null, return error response if so
        if baby:
            # check if diaper is Null, return error response if so
            if diaper:
                # check if toAdd is less than 0, return error response if so
                if toAdd > 0:
                    # Modify diaper and commit to db, return success message. If error, return error response
                    try:
                        diaper.quantity = diaper.quantity + toAdd
                        diaper.modified = datetime.now
                        db.session.commit()
                        return jsonify({
                            'status': 'success',
                            'message': 'New value for size {0}: {1}'.format(diaper.size, diaper.quantity)
                        }), 200
                    except Exception:
                        return jsonError('Something went wrong', 500)
                else:
                    return jsonError('toAdd must be greater than 0', 422)
            else:
                return jsonError('Diaper of that size is not associated with Inventory, add Diaper first', 404)
        else:
            return jsonError('Baby of that id not associated with User, add Baby first', 404)
    else:
        return jsonError('User does not exist', 404)


""" Endpoint to remove n amount of diapers from Diaper in Inventory """
@api.route('/remove', methods=['POST'])
@jwt_required(refresh=False)
def remove():
    """ Takes in POST request with babyId, num, and size of diapers, removes them from the current amount and updates the db """

    data = request.get_json()

    # get user by username from jwt token
    user = User.getByUsername(get_jwt_identity())

    # Get baby object from user.babies. If no match, return error message
    for b in user.babies:
        if b.id == data["babyId"]:
            baby = b

    # Get diaper object from user.babies.inventory.
    try:
        for d in baby.inventory.diapers.getInventory():
            if d.size == data["size"]:
                diaper = d
    except KeyError:
        return jsonError('inventory does not exist, add inventory to baby', 404)
        
    # convert data["toRemove"] to int, if ValueError, return error response
    try:
        toAdd = int(data["toRemove"])
    except ValueError:
        return jsonError('toRemove must be an integer', 422)

    # check if user is Null, return error response if so
    if user:
        # check if baby is Null, return error response if so
        if baby:
            # check if diaper is Null, return error response if so
            if diaper:
                # check if toRemove is less than 0, return error response if so
                if toRemove > 0:
                    # Modify diaper and commit to db, return success message. If error, return error response
                    try:
                        # check if toRemove is less than the current value of diaper.quantity. If not, set quantity to 0
                        if toRemove < diaper.quantity:
                            diaper.quantity = diaper.quantity - toRemove
                        else:
                            diaper.quantity = 0
                        diaper.modified = datetime.now
                        db.session.commit()
                        return jsonify({
                            'status': 'success',
                            'message': 'New value for size {0}: {1}'.format(diaper.size, diaper.quantity)
                        }), 200
                    except Exception:
                        return jsonError('Something went wrong', 500)
                else:
                    return jsonError('toRemove must be greater than 0', 422)
            else:
                return jsonError('Diaper of that size is not associated with Inventory, add Diaper first', 404)
        else:
            return jsonError('Baby of that id not associated with User, add Baby first', 404)
    else:
        return jsonError('User does not exist', 404)


""" Enpoint to create Baby and associate with User """
@api.route('/addbaby', methods=['POST'])
@jwt_required(refresh=False)
def addBaby():
    user = User.getByUsername(get_jwt_identity())

    if user:
        if "firstName" in data:
            if "lastName" in data:
                if "birthday" in data:
                    try:
                        date = datetime.strptime(data['birthday'], 'MM/DD/YYYY')
                    except ValueError:
                        return jsonError('Date must be in MM/DD/YYYY format', 409)
                    if "height" in data:
                        try:
                            height = int(data["height"])
                        except ValueError:
                            return jsonError("Height must be an integer", 422)
                        if "weight" in data:
                            try:
                                weight = int(data["weight"])
                            except ValueError:
                                return jsonError("Weight must be an integer", 422)
                            if "currentSize" in data:
                                # build Baby
                                newBaby = Baby(
                                    userId=user.id,
                                    firstName=data["firstName"],
                                    lastName=data["lastName"],
                                    birthday=date,
                                    height=height,
                                    weight=weight,
                                    currentSize=data["currentSize"],
                                )

                                newBaby.inventory = Inventory(
                                    babyId=newBaby.id, 
                                    diapers=[]
                                )

                                # Commit new Baby to db. If successful, return success message. If not, return error
                                try:
                                    newBaby.commitToDb()
                                    return jsonify({
                                        "status": "success",
                                        'message': "Baby created"
                                    }), 201
                                except Exception:
                                    return jsonError('Something went wrong', 500)

                            else:
                                return jsonError('missing "currentSize" property', 404)
                        else:
                            return jsonError('missing "weight" property', 404)
                    else:
                        return jsonError('missing "height" property', 404)
                else:
                    return jsonError('missing "birthday" property', 404)
            else:
                return jsonError('missing "lastName" property', 404)
        else:
            return jsonError('missing "firstName" property', 404)
    else:
        return jsonError('User does not exist', 404)
