"""
User.py:
    data classes and orm bindings for Users and Babies
"""

from datetime import datetime

# import db
from .Shared import db, jsonError


class User(db.Model):
    """ Defines user attributes and links baby to user """
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.VARCHAR(25), nullable=False)
    email = db.Column(db.VARCHAR(50), nullable=False)
    password = db.Column(db.String(250), nullable=False)
    joined = db.Column(db.DateTime, default=datetime.now)
    firstName = db.Column(db.VARCHAR(50), nullable=False)
    lastName = db.Column(db.VARCHAR(50), nullable=False)
    babies = db.relationship('Baby', backref='user', lazy='joined')

    def toDict(self):
        """ Constructs data into JSON-like format for conversion"""
        return dict(
            username = self.username,
            email = self.email,
            joined = self.joined,
            firstName = self.firstName,
            lastName = self.lastName,
            babies = [baby.toDict() for baby in getattr(self, 'babies')]
        )
    
    @classmethod
    def getByUsername(cls, username):
        """ returns User if username in the class is in the database. Null if not """
        return cls.query.filter(cls.username == username).first()

    @classmethod
    def getByEmail(cls, email):
        """ returns User if email in the class is in the database. Null if not """
        return cls.query.filter(cls.email == email).first()

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()

    def deletefromDb(self):
        db.session.delete(self)
        db.session.commit()


class Baby(db.Model):
    """ Defines baby attributes and links user to baby and inventory"""
    __tablename__ = 'babies'

    id = db.Column(db.Integer, primary_key=True)
    userId = db.Column(db.Integer, db.ForeignKey('users.id'))
    firstName = db.Column(db.VARCHAR(50))
    lastName = db.Column(db.VARCHAR(50))
    inventory = db.relationship('Inventory', backref=db.backref('baby', uselist=False))
    birthday = db.Column(db.DateTime)
    height = db.Column(db.Integer)
    weight = db.Column(db.Integer)
    currentSize = db.Column(db.VARCHAR(3))

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(
        firstName = self.firstName,
        lastName = self.lastName,
        inventory = getattr(self, 'inventory').getInventory(),
        birthday = self.birthday,
        height = self.height,
        weight = self.weight,
        currentSize = self.currentSize
    )

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()

    def deletefromDb(self):
        db.session.delete(self)
        db.session.commit()
