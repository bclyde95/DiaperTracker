"""
Inventory.py
    data classes and orm bindings for Inventories and Diapers
"""

from datetime import datetime

# import db
from .Shared import db


class Inventory(db.Model):
    """ Collection model that aggregates individual Diaper Models and associates the aggregate with a Baby """
    __tablename__ = 'inventories'

    id = db.Column(db.Integer, primary_key=True)
    babyId = db.Column(db.Integer, db.ForeignKey('babies.id'))
    diapers = db.relationship('Diaper', backref='Inventory', lazy='joined')

    def getInventory(self):
        """ Returns an array of diaper dicts """
        return [diaper.toDict() for diaper in getattr(self, "diapers")]


class Diaper(db.Model):
    """ Defines Diaper model and links diaper values to diaper Collection """
    __tablename__ = 'diapers'

    id = db.Column(db.Integer,primary_key=True)
    collectionId = db.Column(db.Integer, db.ForeignKey('inventories.id'))
    size = db.Column(db.VARCHAR(3))
    quantity = db.Column(db.Integer)
    modified = db.Column(db.DateTime, default=datetime.utcnow)

    def toDict(self):
        """ Constructs data into JSON-like format for conversion """
        return dict(
            size = self.size,
            quantity = self.quantity,
            modified = self.modified
        )

    def validSize(self, size):
        sizes = ["00", "0", "1", "2", "3", "4", "5", "6", "7", "8"]
        return size in sizes
        

    def commitToDb(self):
        db.session.add(self)
        db.session.commit()

    def deletefromDb(self):
        db.session.delete(self)
        db.session.commit()
