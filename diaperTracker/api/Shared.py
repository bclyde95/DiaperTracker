"""
Shared.py:
    Database init and shared data classes
"""

from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from re import fullmatch

# init db
db = SQLAlchemy()


def hashPassword(password):
    return generate_password_hash(password)


def checkPassword(hashed, password):
    return check_password_hash(hashed, password)


def isEmail(dataIn):
    emailRE = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    if fullmatch(emailRE, dataIn):
        return True
    return False

def jsonError(message, code):
    return jsonify({
        'status': 'error',
        'message': message
    }), code
