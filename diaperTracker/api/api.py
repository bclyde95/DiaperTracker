"""
api.py:
    routing and api CRUD functions
"""

# library imports
from flask import Blueprint, jsonify, request, render_template
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity

# file imports
from .Shared import db, hashPassword, checkPassword, isEmail, jsonError

from .User import User, Baby
from .Inventory import Inventory, Diaper

api = Blueprint('api', __name__)


###---- User routes ----###\

""" Endpoint to get and set diapers per Baby """
@api.route('/diapers', methods=['GET'])
@api.route('/diapers/<string:babyName>', methods=['GET', 'POST'])
@api.route('/diapers/<string:babyName>/<string:size>', methods=['GET', 'POST'])
@jwt_required(refresh=False)
def diapers(babyName=None, size=None):
    user = User.getByUsername(get_jwt_identity())

    if user:
        if babyName:
            for b in user.babies:
                if babyName == b.firstName:
                    baby = b
                
            
                if baby:
                    if size:
                        if Diaper.validSize(size):
                            # Check if baby has size already. If so, save that Diaper object as diaper
                            for d in baby.inventory.diapers:
                                if size == d.size:
                                    diaper = d
                            # GET method
                            if request.method == "GET":
                                if diaper:
                                    return jsonify({
                                        'status': 'success',
                                        babyName: {
                                            'diapers': diaper.toDict()
                                        }
                                    }), 200
                                else:
                                    return jsonError('No diapers of that size', 404)
                            # POST method
                            elif request.method == "POST":
                                if "action" in data:
                                    if data["action"] == "create":
                                        if diaper:
                                            return jsonError("Diaper size already associated with Baby. Use update action", 409)
                                        else:
                                            if "quantity" in data:
                                                try:
                                                    amount = int(data["quantity"])
                                                except ValueError:
                                                    return jsonError("Quantity must be an integer", 422)

                                                baby.inventory.diapers.append(Diaper(
                                                collectionId=baby.inventory.id,
                                                size=size,
                                                quantity=amount
                                                ))

                                                try:
                                                    baby.commitToDb()
                                                    return jsonify({
                                                        "status": "success",
                                                        "message": "Size {0} diaper successfully added".format(size)
                                                    }), 201
                                                except Exception:
                                                    return jsonError("Something went wrong", 500)
                                            else:
                                                return jsonError('missing "quantity" property', 404)
                                    elif data["action"] == "update":
                                        if diaper:
                                            if "quantity" in data:
                                                try:
                                                    newQuantity = int(data["quantity"])
                                                except ValueError:
                                                    return jsonError("Quantity must be an integer", 422)

                                                diaper.quantity = newQuantity
                                                
                                                try:
                                                    diaper.commitToDb()
                                                    return jsonify({
                                                        "status":"success",
                                                        "message":"Size {0} updated to {1}".format(size, newQuantity)
                                                    }), 200
                                                except Exception:
                                                    return jsonError("Something went wrong", 500)
                                            else:
                                                jsonError('missing "quantity" property', 404)
                                        else:
                                            jsonError("Diaper size is not associated with baby", 404)
                                    elif data["action"] == "delete":
                                        if diaper:
                                            try:
                                                diaper.deletefromDb()
                                                return jsonify({
                                                    "status":"success",
                                                    "message": "Deleted size {0} diapers from {1}'s inventory".format(size, babyName)
                                                }), 200
                                        else:
                                            jsonError("Diaper size is not associated with baby", 404)
                                    else:
                                        return jsonError('Invalid action, must be update or delete', 422)
                                else:
                                    return jsonError('missing "action" property', 404)
                            else:
                                return jsonError('Invalid Method, must be GET or POST', 405)
                        else:
                            return jsonError("Invalid size, must be in [00, 0, 1, 2, 3, 4, 5, 6, 7, 8]", 422)
                    else:
                        # GET method
                        if request.method == "GET":
                            return jsonify({
                                'status': 'success',
                                babyName: {
                                    'diapers': baby.inventory.getInventory()
                                }
                            }), 200
                        # POST method
                        elif request.method == "POST":
                            if "action" in data:
                                # Create diapers from list of Diaper objects
                                if data["action"] == "create":
                                    if "diapers" in data:
                                        for diaper in data["diapers"]:
                                            if "size" in diaper:
                                                if "quantity" in diaper:
                                                    try:
                                                        amount = int(diaper["quantity"])
                                                    except ValueError:
                                                        return jsonError("Quantity must be an integer", 422)
                                                    # Build diaper object array
                                                    baby.inventory.diapers.append(Diaper(
                                                        collectionId=baby.inventory.id,
                                                        size=diaper["size"],
                                                        quantity=amount
                                                    ))
                                                else:
                                                    return jsonError('missing "quantity" property in diaper', 404)
                                            else:
                                                return jsonError('missing "size" property in diaper', 404)
                                        try:
                                            baby.commitToDb()
                                            return jsonify({
                                                "status": "success",
                                                "message": "Diapers successfully added"
                                            }), 201
                                        except Exception:
                                            return jsonError("Something went wrong", 500)
                                    else:
                                        return jsonError('missing "diapers" property', 404)
                                # Delete all diapers from Baby's inventory
                                elif data["action"] == "delete":
                                    for d in baby.inventory.diapers:
                                        try:
                                            d.deletefromDb()
                                        except Exception:
                                            return jsonError("Error occured while deleting diapers", 500)
                                    return jsonify({
                                        "status": "success",
                                        "message": "Successfully deleted all diapers associated with {0}".format(baby.firstName)
                                    }) 200
                                else:
                                    return jsonError('Invalid action, must be update or delete', 422)
                            else:
                                return jsonError('missing "action" property', 404)
                        else:
                            return jsonError('Invalid Method, must be GET or POST', 405)
                else:
                    return jsonError('Baby is not associated with user', 404)
        else:
            # if just diapers, return inventories for all babies registered to user
            # build response dict
            output = {
                'status': 'success'
            }
            try:
                for b in user.babies:
                    output[baby.firstName] = {
                        'diapers': b.inventory.getInventory()
                    }
            except KeyError:
                return jsonError('No babies registered to User')
            return jsonify(output)
                
    else:
        return jsonError('User does not exist', 404)


""" Endpoint to get and set user information """
@api.route('/<string:username>', methods=['GET', 'POST'])
@jwt_required(refresh=False)
def user(username):
    user = User.getByUsername(username)
    
    if user:
        # GET method
        if request.method == 'GET':
            return jsonify({
                'status': 'success',
                'data': user.toDict()
            })
        # POST Method
        elif request.method == 'POST':
            data = request.get_json()
            if data["action"] == "update":
                updated = []
                # iterate through dict, looking for keys that match the updatable fields
                if 'username' in data:
                    # Checks if username exists and returns error if so
                    if User.getByUsername(data["username"]):
                        return jsonError('Username already in use', 409)
                    else:
                        # update username and append key to updated
                        user.username = data["username"]
                        updated.append(key)
                if 'email' in data:
                    # Checks if email exists and returns error if so
                    if User.getByEmail(data["email"]):
                        return jsonError('Email already in use', 409)
                    else:
                        # update email and append key to updated
                        user.email = data["email"]
                        updated.append(key)
                if 'firstName' in data:
                    # update firstName and append key to updated
                    user.firstName = data["firstName"]
                    updated.append(key)
                if 'lastName' in data:
                    # update lastName and append key to updated
                    user.lastName = data["lastName"]
                    updated.append(key)
                # try to commit to db, error if failed
                try:
                    user.commitToDb()
                    return jsonify({
                        'status': 'success',
                        'message': 'Changes saved for {0}'.format(', '.join(updated))
                    }), 200
                except Exception:
                    return jsonError('Something went wrong', 500)
            elif data["action"] == "delete":
                try:
                    user.deletefromDb()
                    return jsonify({
                        'status': 'success',
                        'message': 'User deleted'
                    }), 200
                except Exception:
                    return jsonError('Something went wrong', 500)
            else:
                return jsonError('Invalid action, must be update or delete', 422)
        else:
            return jsonError('Invalid Method, must be GET or POST', 405)               
    else:
        return jsonError('User does not exist', 404)



""" Endpoint to get and set a user's baby information """
@api.route('/<string:username>/<string:babyName>', methods=['GET', 'POST'])
@jwt_required(refresh=False)
def baby(username, babyName):
    user = User.getByUsername(username)
    
    if user:
        # find baby in babies associated with user
        for b in user.babies:
            if babyName == b.firstName:
                baby = b

        if request.method == 'GET':
            if baby:
                return jsonify({
                    'status': 'success',
                    'data': baby.toDict()
                })
            else:
                return jsonError("Baby does not exist", 404)               

        elif request.method == 'POST':
            data = request.get_json()
            if "action" in data:
                if data["action"] == "create":
                    if "lastName" in data:
                        if "birthday" in data:
                            try: 
                                date = datetime.strptime(data["birthday"], "MM/DD/YYYY")
                            except ValueError:
                                return jsonError("Date must be in MM/DD/YYYY format", 409)
                            if "height" in data:
                                try:
                                    weight = int(data["height"])
                                except ValueError:
                                    return jsonError("Height must be an integer", 422)
                                if "weight" in data:
                                    try:
                                        weight = int(data["weight"])
                                    except ValueError:
                                        return jsonError("Weight must be an integer", 422)
                                    if "currentSize" in data:
                                        # build Baby
                                        newBaby = Baby(
                                            userId=user.id,
                                            firstName=babyName,
                                            lastName=data["lastName"],
                                            birthday=date,
                                            height=height,
                                            weight=weight,
                                            currentSize=data["currentSize"],
                                        )

                                        newBaby.inventory = Inventory(
                                            babyId=newBaby.id, 
                                            diapers=[]
                                        )

                                        # Commit new Baby to db. If successful, return success message. If not, return error
                                        try:
                                            newBaby.commitToDb()
                                            return jsonify({
                                                "status": "success",
                                                'message': "Baby {0} created".format(babyName)
                                            }), 201
                                        except Exception:
                                            return jsonError('Something went wrong', 500)

                                    else:
                                        return jsonError('missing "currentSize" property', 404)
                                else:
                                    return jsonError('missing "weight" property', 404)
                            else:
                                return jsonError('missing "height" property', 404)
                        else:
                            return jsonError('missing "birthday" property', 404)
                    else:
                        return jsonError('missing "lastName" property', 404)

                elif data["action"] == "update":
                    if baby:
                        updated = []
                        # iterate through dict, looking for keys that match the updatable fields
                        if 'firstName' in data:
                            # update firstName and append key to updated
                            baby.firstName = data["firstName"]
                            updated.append(key)
                        if 'lastName' in data:
                            # update lastName and append key to updated
                            baby.lastName = data["lastName"]
                            updated.append(key)
                        if 'birthday' in data:
                            # try to convert date string to date, error if not in valid format
                            try:
                                date = datetime.strptime(data['birthday'], 'MM/DD/YYYY')
                            except ValueError:
                                return jsonError('Date must be in MM/DD/YYYY format', 409)
                            # update birthday and append key to updated
                            baby.birthday = date
                            updated.append(key)
                        if 'height' in data:
                            # Convert height string to integer, if ValueError return error response
                            try:
                                height = int(data["height"])
                            except ValueError:
                                return jsonError('Height must be an integer in inches')
                            # update height and append key to updated
                            baby.height = height
                            update.append(key)
                        if 'weight' in data:
                            # Convert weight string to integer, if ValueError return error response
                            try:
                                weight = int(data["weight"])
                            except ValueError:
                                return jsonError('Weight must be an integer in lbs')
                            # update height and append key to updated
                            baby.weight = weight
                            update.append(key)
                        if 'currentSize' in data:
                            # update currentSize and append key to updated
                            baby.currentSize = data["currentSize"]
                            update.append(key)
                        if 'image' in data:
                            # update image and append key to updated
                            baby.image = data["image"]
                            update.append(key)
                        # try to commit to db, error if failed
                        try:
                            user.commitToDb()
                            return jsonify({
                                'status': 'success',
                                'message': 'Changes saved for {0}'.format(', '.join(updated))
                            }), 200
                        except Exception:
                            return jsonError('Something went wrong', 500)
                    else:
                        return jsonError("Baby does not exist", 404)               

                elif data["action"] == 'delete':
                    if baby:
                        try:
                            baby.deletefromDb()
                            return jsonify({
                                'status': 'success',
                                'message': "Baby deleted"
                            }), 200
                        except Exception:
                            return jsonError('Something went wrong', 500)
                    else:
                        return jsonError("Baby does not exist", 404)               
                else:
                    return jsonError('Invalid action, must be create, update, or delete', 422)
            else:
                return jsonError('missing "action" property', 404)
        else:
            return jsonError('Invalid Method, must be GET or POST', 405)
    else:
        return jsonError('User does not exist', 404)

###--------------###


###---- AUTH ----###

""" Endpoint for users to login and recieve an access token """
@api.route('/auth/login', methods=['POST'])
def login():
    """ Takes in POST request with login info, veriifies the login, and returns API access and refresh tokens """
    data = request.get_json()
    
    # Determine if userIn is email or username
    # Then tries to find User in database, stores Null in user if not.
    if isEmail(data["userIn"]):
        user = User.getByEmail(data["userIn"])
    else:
        user = User.getByUsername(data["userIn"])
    
    # if user is in database, check password. If not, return User does not exist error.
    if user:
        # Checks password with password in db, returns access and refresh token if successful. Error if not.
        if checkPassword(user.password, data['password']):
            access_token = create_access_token(identity = user.id)
            refresh_token = create_refresh_token(identity = user.id)
            return jsonify({
                "status": "success",
                "accessToken": access_token,
                "refreshToken":
                refresh_token
            }), 200
        else:
            return jsonError('Incorrect password', 401)
    if not user:
        return jsonError('User does not exist', 404) 
    else:
        return jsonError('Something went wrong', 500)


""" Endpoint for users to register and recieve intial API token """
@api.route('/auth/register', methods=['POST'])
def register():
    """ Takes in POST request with registration info, checks if username and email are free, creates a db entry, and returns API access and refresh tokens """
    data = request.get_json()

    # Checks if username exists and returns error if so
    if User.getByUsername(data["username"]):
        return jsonError('Username already in use', 409)
    
    # Checks if email exists and returns error if so
    if User.getByEmail(data["email"]):
        return jsonError('Email already in use', 409)

    # initialize new User
    newUser = User(
        username=data["username"],
        email=data["email"],
        password=hashPassword(data["password"]),
        firstName=data["firstName"],
        lastName=data["lastName"],
        babies=[]
    )

    # Commit data to db. If successful, return access and refresh tokens. If not, return error
    try:
        newUser.commitToDb()
        access_token = create_access_token(identity = newUser.username)
        refresh_token = create_refresh_token(identity = newUser.username)
        return jsonify({
            'status': 'success',
            'accessToken': access_token,
            'refreshToken': refresh_token
        }), 201
    except ValueError:
        return jsonError('Something went wrong', 500)


@api.route('/auth/tokenrefresh', methods=['POST'])
@jwt_required(refresh=True)
def tokenRefresh(self):
    """ Takes in refresh token and returns new access token """
    username = get_jwt_identity()
    access_token = create_access_token(identity = username)

    # try to return access token, if error return error
    try:
        return jsonify({
            'status': 'success',
            'accessToken': access_token
        }), 201
    except Exception:
        return jsonError('Something went wrong', 500)

###---------------------###
