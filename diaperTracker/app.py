"""
app.py:
    initializes and registers app components,
    loads configuration,
    sets up migration util
"""

# library imports
from flask import Flask
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager

# file imports
from api.Shared import db
from api.api import api


# initialize flask app
app = Flask("DiaperTracker")

# load config from config file
app.config.from_object('config.Config')

# Index route that serves React app
""" Base route to serve React front end """
@app.route('/', methods=['GET'])
@app.errorhandler(404)
def react():
    """ Serves index.html template that react renders to, lets React handle errors """
    return render_template("index.html")


#register api blueprint
app.register_blueprint(api, url_prefix="/api")

# initialize database
db.init_app(app)

# setup migrations
migrate = Migrate(app, db)

jwt = JWTManager(app)
