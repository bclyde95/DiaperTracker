"""
config.py:
    config object for flask
"""

class Config(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'fillThisIn.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    STATIC_URL_PATH = '/react/build/'
    STATIC_FOLDER = 'build'
